package saveme.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SQLServerConnUtils_SQLJDBC {
	public static Connection getSQLServeConnection() throws ClassNotFoundException, SQLException {
		String hostName = "localhost";
		String sqlInstanceName = "SQLEXPRESS";
		String database = "SaveMeJava";
		String userName = "sa";
		String password = "root";
		
		return getSQLServerConnection(hostName, sqlInstanceName, database, userName, password);
	}
	
	public static Connection getSQLServerConnection(String hostName, String sqlInstanceName, String database, String userName, String password) throws SQLException, ClassNotFoundException {
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		
		String connectionURL = "jdbc:sqlserver://" + hostName + ":1433" + ";instance=" +  sqlInstanceName + ";databaseName=" + database;
		
		Connection conn = DriverManager.getConnection(connectionURL, userName, password);
		return conn;
	}
}
