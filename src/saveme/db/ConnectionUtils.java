package saveme.db;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionUtils {

	public static Connection getConnection() throws SQLException, ClassNotFoundException {
		return SQLServerConnUtils_SQLJDBC.getSQLServeConnection();
	}

	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		System.out.println("Get connection ...");

		Connection conn = ConnectionUtils.getConnection();

		System.out.println("Get connection " + conn);

		System.out.println("Done!");
	}

	public static void closeQuietly(Connection conn) {
		try {
			conn.close();
		} catch (Exception e) {
		}
	}

	public static void rollbackQuietly(Connection conn) {
		try {
			conn.rollback();
		} catch (Exception e) {
		}
	}
}
