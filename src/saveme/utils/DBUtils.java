package saveme.utils;
 
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
 
import saveme.beans.Product;
import saveme.beans.UserAccount;
 
public class DBUtils {
 
    public static UserAccount findUser(Connection conn, //
            String userName, String password) throws SQLException {
    	Statement st = conn.createStatement();
        String sql = "Select a.User_Name, a.Password, a.Gender from User_Account a " //
                + " where a.User_Name = '"+ userName +"' and a.password= '"+ password +"'";
        ResultSet rs = st.executeQuery(sql);
 
        if (rs.next()) {
            String gender = rs.getString("Gender");
            UserAccount user = new UserAccount();
            user.setUserName(userName);
            user.setPassword(password);
            user.setGender(gender);
            return user;
        }
        return null;
    }
 
    public static UserAccount findUser(Connection conn, String userName) throws SQLException {
    	Statement st = conn.createStatement();
        String sql = "Select User_Name, Password, Gender from User_Account "//
                + " where User_Name = '"+ userName +"' ";

        ResultSet rs = st.executeQuery(sql);
 
        if (rs.next()) {
            String password = rs.getString("Password");
            String gender = rs.getString("Gender");
            UserAccount user = new UserAccount();
            user.setUserName(userName);
            user.setPassword(password);
            user.setGender(gender);
            return user;
        }
        return null;
    }
 
    public static void changePassword(Connection conn, UserAccount user) throws SQLException {
        Statement st = conn.createStatement();
    	String sql = "Update User_Account set Password = '"+user.getPassword()+"' where User_name='"+ user.getUserName() +"' ";
    	
        st.executeUpdate(sql);
    }
    
    public static List<Product> queryProduct(Connection conn) throws SQLException {
        String sql = "Select Code, Name, Price from Product ";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
 
        ResultSet rs = pstm.executeQuery();
        List<Product> list = new ArrayList<Product>();
        while (rs.next()) {
            String code = rs.getString("Code");
            String name = rs.getString("Name");
            float price = rs.getFloat("Price");
            Product product = new Product();
            product.setCode(code);
            product.setName(name);
            product.setPrice(price);
            list.add(product);
        }
        return list;
    }
 
    public static Product findProduct(Connection conn, String code) throws SQLException {
        String sql = "Select a.Code, a.Name, a.Price from Product a where a.Code=?";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, code);
 
        ResultSet rs = pstm.executeQuery();
 
        while (rs.next()) {
            String name = rs.getString("Name");
            float price = rs.getFloat("Price");
            Product product = new Product(code, name, price);
            return product;
        }
        return null;
    }
 
    public static void updateProduct(Connection conn, Product product) throws SQLException {
        Statement st = conn.createStatement();
    	String sql = "Update Product set Name ='"+ product.getName() +"', Price="+ product.getPrice() +" where Code='"+ product.getCode() +"'";
 
    	st.executeUpdate(sql);
    }
 
    public static void insertProduct(Connection conn, Product product) throws SQLException {
        Statement st = conn.createStatement(); 
    	String sql = "Insert into Product(Code, Name,Price) values ('"+product.getCode()+"','"+product.getName()+"','"+product.getPrice()+"')";

        st.executeUpdate(sql);
    }
 
    public static void deleteProduct(Connection conn, String code) throws SQLException {
        String sql = "Delete From Product where Code= ?";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
 
        pstm.setString(1, code);
 
        pstm.executeUpdate();
    }
 
}