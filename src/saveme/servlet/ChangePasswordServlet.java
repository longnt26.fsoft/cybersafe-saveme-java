package saveme.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import saveme.beans.UserAccount;
import saveme.utils.DBUtils;
import saveme.utils.MyUtils;

@WebServlet(urlPatterns = { "/changePassword" })
public class ChangePasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public ChangePasswordServlet() {
		super();
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Connection conn = MyUtils.getStoredConnection(request);
		
		String username = (String) request.getParameter("username");
		
		UserAccount user = null;
		
		String errorString = null;
		
		try {
			user = DBUtils.findUser(conn, username);
		} catch (SQLException e) {
			e.printStackTrace();
			errorString = e.getMessage();
		}
		
		if (errorString != null && user == null) {
			response.sendRedirect(request.getServletPath() + "/userInfo");
			return;
		}
		
		request.setAttribute("errorString", errorString);
		request.setAttribute("user", user);
		
		RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/WEB-INF/views/changePassword.jsp");
		dispatcher.forward(request, response);	
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		Connection conn = MyUtils.getStoredConnection(request);
		
		String username = (String) request.getParameter("username");
		String password = (String) request.getParameter("password");
		String confirmPass = (String) request.getParameter("confirmPass");
		
		UserAccount user = new UserAccount(username, password);
		
		String errorString = null;
		
		try {
			DBUtils.changePassword(conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
			errorString = e.getMessage();
		}
		request.setAttribute("errorString", errorString);
		request.setAttribute("username", username);
		
		if(errorString != null) {
			RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/WEB-INF/views/changePassword.jsp");
			dispatcher.forward(request, response);
		} else {
			response.sendRedirect(request.getContextPath() + "/userInfo");
		}
	}
}
