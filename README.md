# CyberSafe-SaveMe-Java

Java version of Save Me Web App - Vulnerable Web Application to practice secure coding skill with basic vulnerabilities (OWASP Top 10 2013 - A1, A2, A3, A4, and A8)

1. Clone the project
2. Use SaveMeDB.sql to set up local DB - using SQL Server
3. Rebuild the project and start your practice

Note: Config database attributes to appropriate with your local db in "SaveMeJav/src/saveme/db/SQLServerConnUtils_SQLJDBC.java" which includes
        String hostName = "localhost";
		String sqlInstanceName = "SQLEXPRESS";
		String database = "SaveMeJava";
		String userName = "sa";
		String password = "root";
