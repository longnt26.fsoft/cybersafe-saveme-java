<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Delete Product</title>
<jsp:include page="_style.jsp"></jsp:include>
</head>

<body>

	<jsp:include page="_header.jsp"></jsp:include>
	<div id="wrapper">
		<jsp:include page="_menu.jsp"></jsp:include>
		<div id="content-wrapper">
			<div class="container-fluid">
				<div class="row justify-content-left">
					<div class="col-x1-12" style="margin-left: 20px;">
						<h3>Delete Product</h3>

						<p style="color: red;">${errorString}</p>
						<a href="productList">Product List</a>
					</div>
				</div>
			</div>
			<jsp:include page="_footer.jsp"></jsp:include>
		</div>
	</div>
</body>
</html>