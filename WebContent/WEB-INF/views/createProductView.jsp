<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Create Product</title>
<jsp:include page="_style.jsp"></jsp:include>
</head>
<body>

	<jsp:include page="_header.jsp"></jsp:include>
	<div id="wrapper">
		<jsp:include page="_menu.jsp"></jsp:include>
		<div id="content-wrapper">
			<div class="card mb-3">
				<div class="card-header">
					<h3>Create New Product</h3>
				</div>
				<div class="card-body">
					<p class="text-danger">${errorString}</p>

					<form method="POST"
						action="${pageContext.request.contextPath}/createProduct">
						<div class="form-horizontal">

							<div class="form-group">
								<label class="control-label col-md-4">Code</label> <input
									class="form-control col-md-10" type="text" name="code"
									value="${product.code}" />
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Name</label> <input
									class="form-control col-md-10" type="text" name="name"
									value="${product.name}" />
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Price</label> <input
									class="form-control col-md-10" type="text" name="price"
									value="${product.price}" />
							</div>
							<div class="form-group">
								<div class="col-md-offset-2 col-md-10">
									<input class="btn btn-primary" type="submit" value="Submit" /> 
									<a class="btn btn-default" href="productList">Cancel</a>
									</td>
								</div>
							</div>

						</div>
					</form>
				</div>
			</div>
			<jsp:include page="_footer.jsp"></jsp:include>
		</div>
	</div>
</body>
</html>