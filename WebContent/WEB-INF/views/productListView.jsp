<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Product List</title>
<jsp:include page="_style.jsp"></jsp:include>
</head>
<body>

	<jsp:include page="_header.jsp"></jsp:include>
	<div id="wrapper">
		<jsp:include page="_menu.jsp"></jsp:include>
		<div id="content-wrapper">
			<div class="card mb-3">
				<div class="card-header">
					<h3>Product List</h3>
				</div>
				<div class="card-body col-lg-10" >
					<a class="btn btn-success" href="createProduct">Create Product</a>
					<p style="color: red;">${errorString}</p>
					<div class="table-responsive">
						<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
							<tr>
								<th>Code</th>
								<th>Name</th>
								<th>Price</th>
								<th>Action</th>						
							</tr>
							<c:forEach items="${productList}" var="product">
								<tr>
									<td>${product.code}</td>
									<td>${product.name}</td>
									<td>${product.price}</td>
									<td><a href="editProduct?code=${product.code}" class="btn btn-warning">Edit</a>
									<a href="deleteProduct?code=${product.code}" class="btn btn-danger">Delete</a>
									</td>
								</tr>
							</c:forEach>
						</table>
					</div>
				</div>
			</div>
			<jsp:include page="_footer.jsp"></jsp:include>
		</div>
	</div>
</body>
</html>