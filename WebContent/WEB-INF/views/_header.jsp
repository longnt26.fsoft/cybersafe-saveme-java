<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<nav class="navbar navbar-expand navbar-dark bg-dark static-top">
	<a class="navbar-brand mr-1" href="${pageContext.request.contextPath}/"><b>SAVE
			ME</b></a>
	<button class="btn btn-link btn-sm text-white order-1 order-sm-0"
		id="sidebarToggle" href="#">
		<i class="fas fa-bars"></i>
	</button>
	<!-- Navbar Search -->
	<div
		class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
	</div>
	<!-- Navbar -->
	<ul class="navbar-nav ml-auto ml-md-0">
		<li class="nav-item dropdown no-arrow"><a
			class="nav-link dropdown-toggle" href="#" id="userDropdown"
			role="button" data-toggle="dropdown" aria-haspopup="true"
			aria-expanded="false"> <i class="fas fa-user-circle fa-fw"></i>
		</a>
			<div class="dropdown-menu dropdown-menu-right"
				aria-labelledby="userDropdown">
				<!-- User store in session with attribute: loginedUser -->

<c:if test="${empty loginedUser}">
				<a class="dropdown-item" href="${pageContext.request.contextPath}/login">Login</a>
</c:if>
<c:if test="${not empty loginedUser}">
				<p class="dropdown-item">Hello ! ${loginedUser.userName}</p>
				<a class="dropdown-item"
					href="${pageContext.request.contextPath}/login">Logout</a>
</c:if>

			</div></li>
	</ul>
</nav>