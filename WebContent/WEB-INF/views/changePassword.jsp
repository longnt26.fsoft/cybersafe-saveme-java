<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Change Password</title>
<jsp:include page="_style.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="_header.jsp"></jsp:include>
	<div id="wrapper">
		<jsp:include page="_menu.jsp"></jsp:include>
		<div id="content-wrapper">
			<div class="card mb-3">
			    <div class="card-header">
			        <h3>Change Password</h3>
			    </div>
			    <div class="card-body">
					<p style="color: red;">${errorString}</p>
					<c:if test="${not empty user}">
						<form method="POST" action="${pageContext.request.contextPath}/changePassword">
							<div class="form-horizontal">
							<input type="hidden" name="username" value="${user.userName}" />
							
								<div class="form-group">
									<label class="control-label col-md-3">New Password</label>
									<input class="form-control col-md-3" type="password" name="password" value="" />
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Confirm Password</label>
									<input class="form-control col-md-3" type="password" name="confirmPass" value="" />
								</div>
								<div class="form-group">
									<div class="col-md-10">
										<input class="btn btn-primary" type="submit" value="Submit"/>
										<a class="btn btn-default" href="${pageContext.request.contextPath}/userInfo">Cancel</a>
									</div>									
								</div>						
							</div>
						</form>
					</c:if>
					</div>
				</div>
			<jsp:include page="_footer.jsp"></jsp:include>
		</div>
	</div>
</body>
</html>