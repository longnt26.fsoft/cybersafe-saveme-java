<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
  
<!-- Sidebar -->
  <ul class="sidebar navbar-nav">
      <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/">
              <i class="fas fa-fw fa-home"></i>
              <span>Home</span>
          </a>
      </li>
      <li class="nav-item active">
          <a class="nav-link" href="${pageContext.request.contextPath}/productList">
              <i class="fas fa-fw fa-box-open"></i>
              <span>Manage Products</span>
          </a>
      </li>
      <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/userInfo">
              <i class="fas fa-fw fa-user"></i>
              <span>My Account Info</span>
          </a>
      </li>
  </ul>
	