<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>User Info</title>
<jsp:include page="_style.jsp"></jsp:include>
</head>
<body>

	<jsp:include page="_header.jsp"></jsp:include>
	<div id="wrapper">
		<jsp:include page="_menu.jsp"></jsp:include>
		<div id="content-wrapper">
			<div class="card mb-3">
				<div class="card-header">
					<h3>Hello, ${user.userName}</h3>
				</div>
				<div class="card-body">
					<div class="col-md-5">
					User Name: <b>${user.userName}</b>
					</div> <br /> 
					<div class="col-md-5">
					Gender: ${user.gender }
					</div>
					<br /> <br> 
					<a class="btn btn-outline-info" href="changePassword?username=${user.userName}">Change Password</a>
				</div>
			</div>
			<jsp:include page="_footer.jsp"></jsp:include>
		</div>
	</div>
</body>
</html>