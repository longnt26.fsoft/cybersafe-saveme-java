<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Edit Product</title>
<jsp:include page="_style.jsp"></jsp:include>
</head>
<body>

	<jsp:include page="_header.jsp"></jsp:include>
	<div id="wrapper">
		<jsp:include page="_menu.jsp"></jsp:include>
		<div id="content-wrapper">
			<div class="card mb-3">
				<div class="card-header">
					<h3>Edit Product</h3>
				</div>
				<div class="card-body">
					<p style="color: red;">${errorString}</p>
					<c:if test="${not empty product}">
						<form method="POST"
							action="${pageContext.request.contextPath}/editProduct">
							<input type="hidden" name="code" value="${product.code}" />
							<div class="form-horizontal">
								<div class="form-group">
									<label class="control-label col-md-3"><b>Product Code:  </b><span class="text-danger">${product.code}</span></label> 
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Product Name</label> <input
										class="form-control col-md-10" type="text" name="name"
										value="${product.name}" />
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Price</label> <input
										class="form-control col-md-10" type="text" name="price"
										value="${product.price}" />
								</div>
								<div class="form-group">
									<div class="col-md-offset-2 col-md-10">
										<input class="btn btn-warning" type="submit" value="Submit" /> 
										<a class="btn btn-default" href="${pageContext.request.contextPath}/productList">Cancel</a>							
									</div>
								</div>
							</div>
						</form>
					</c:if>
				</div>
			</div>
			<jsp:include page="_footer.jsp"></jsp:include>
		</div>
	</div>
</body>
</html>